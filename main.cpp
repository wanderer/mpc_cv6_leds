#include <iostream>
#include "simul_kl25z.h"    // simulator
#include "leds.h"           // nas hlavickovy soubor - ovladac LED.

using namespace std;

int main()
{
    LEDS_Init();

    // blikani LED
    while(1) {
      LEDS_GreenOn();
      system("sleep 1");
      LEDS_BlueOn();
      system("sleep 1");
      LEDS_GreenOff();
      system("sleep 1");
      LEDS_BlueOff();
      system("sleep 1");
    }
    return 0;
}
