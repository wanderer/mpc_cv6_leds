/*
 * leds.c
 *
 *  This is driver for the LEDs on evaluation board for FRDM-KL25Z.
 */
#include <cstdio>

/* Include the main definitions for our MCU */
//#include "MKL25Z4.h"

/* Include the header file for this driver */
#include "leds.h"

/* Include GPIO driver which we will use internally */
//#include "drv_gpio.h"
#include "simul_kl25z.h"


void LEDS_Init(void) {

  /* Enable port clock, set pin function to GPIO*/
  GPIO_Initialize();

  /* Configure pins as output*/
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  pinMode(LED_RED, OUTPUT);


  /* Turn off the LEDs; they are turned off by HIGH value. */
  pinWrite(LED_GREEN, HIGH);
  pinWrite(LED_BLUE, HIGH);
  pinWrite(LED_RED, HIGH);

}

/* Turn on green LED (RGB LED on FRDM-KL25Z board) */
void LEDS_GreenOn(void) {

  pinWrite(LED_GREEN, LOW);

}

void LEDS_BlueOn(void) {

  pinWrite(LED_BLUE, LOW);

}

void LEDS_RedOn(void) {

  pinWrite(LED_RED, LOW);

}


/* Turn off green LED (RGB LED on FRDM-KL25Z board) */
void LEDS_GreenOff(void) {

  pinWrite(LED_GREEN, HIGH);

}

void LEDS_BlueOff(void) {

  pinWrite(LED_BLUE, HIGH);

}

void LEDS_RedOff(void) {

  pinWrite(LED_RED, HIGH);

}
