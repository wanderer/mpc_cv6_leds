/*
 * leds.h
 *
 *  This is driver for the LEDs on evaluation board for FRDM-KL25Z.
 *  To use this driver:T
 *  1) Include this file into your source file (.c file): #include "drv_leds.h"
 *  2) Call LEDs_Init() in your program.
 *  3) Call other functions of this driver as needed.
 *
 *  NOTE: this driver uses drv_gpio.h internally; add it to your project too.
 */

#ifndef SOURCES_DRV_LEDS_H_
#define SOURCES_DRV_LEDS_H_

/* Initialize this driver */
void LEDS_Init(void);

/* Turn on green LED (RGB LED on FRDM-KL25Z board) */
void LEDS_GreenOn(void);

void LEDS_BlueOn(void);

void LEDS_RedOn(void);

/* Turn off green LED (RGB LED on FRDM-KL25Z board) */
void LEDS_GreenOff(void);

void LEDS_BlueOff(void);

void LEDS_RedOff(void);

#endif /* SOURCES_DRV_LEDS_H_ */

