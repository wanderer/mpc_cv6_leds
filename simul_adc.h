/* Simulacni kod pro simulaci AD prevodniku */

#ifndef SIMUL_ADC_H
#define SIMUL_ADC_H
#include "simul_kl25z.h"
#include <stdint.h>


void AdcValueWrite(unsigned int data);
void AdcStartConversion(unsigned int data);

class ADC_Peripheral {
public:
    ADC_Peripheral() : CFG1(&AdcValueWrite), CFG2(&AdcValueWrite), CFG3(&AdcValueWrite),
        SC1{ Property<uint32_t>(&AdcStartConversion), Property<uint32_t>(&AdcValueWrite) },
        R{ Property<uint32_t>(&AdcValueWrite), Property<uint32_t>(&AdcValueWrite) }
    {
        mConversionStarted = false;
        mCurrentDataIndex = 0;
    }

    // monitored values
    Property<uint32_t> CFG1, CFG2, CFG3;
    Property<uint32_t> SC1[2];
    Property<uint32_t> R[2];
    // not monitored values
    uint32_t SC2;
    uint32_t SC3;
    uint32_t CLP0;
    uint32_t CLP1;
    uint32_t CLP2;
    uint32_t CLP3;
    uint32_t CLP4;
    uint32_t CLPS;
    uint32_t CLM0;
    uint32_t CLM1;
    uint32_t CLM2;
    uint32_t CLM3;
    uint32_t CLM4;
    uint32_t CLMS;
    uint32_t PG;
    uint32_t MG;

    // helpers
    bool mConversionStarted;
    int mCurrentDataIndex;

    // called when user writes to a register, to update internal state
    void UpdateData();

    // Test pin on port C is configured as input
    bool IsPinConfigValid() {
        // clock enabled for port C
        if ( (SIM->SCGC5 & SIM_SCGC5_PORTC_MASK) == 0 ) {
            printf("Error: Clock for the port of ADC input pin it not enabled.");
            return false;
        }

        // pin function set to ADC
        if ( (PORTC->PCR[2] & PORT_PCR_MUX_MASK) != 0 ) {
            printf("Error: ADC input pin function not configured for ADC");
            return false;
        }

        return true;

    }

};

#endif // SIMUL_ADC_H
